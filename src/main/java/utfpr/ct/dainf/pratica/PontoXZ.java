/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1949772
 */
public class PontoXZ extends Ponto2D {

    public PontoXZ() {
        super();
    }

    public PontoXZ(double xi, double zi) {
        super(xi, 0.0, zi);
    }
    
    @Override
    public String toString(){
        return this.getNome()+String.format("(%f,%f)",this.getX(),this.getZ());
    }
}
