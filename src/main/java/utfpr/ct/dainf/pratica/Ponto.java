package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    public Ponto() {
        x = 0.0;
        y = 0.0;
        z = 0.0;
    }
    
    public Ponto(double xi, double yi, double zi){
        x = xi;
        y = yi;
        z = zi;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    @Override
    public String toString(){
        return this.getNome()+String.format("(%f,%f,%f)",this.x,this.y,this.z);
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Ponto){
            return ((Ponto) obj).getX() == this.x
                    && ((Ponto) obj).getY() == this.y
                    && ((Ponto) obj).getZ() == this.z;
        }
        else {
            return false;
        }
    }
    
    public double dist(Ponto dot){
        return Math.sqrt(Math.pow(dot.getX()-this.x, 2)
                + Math.pow(dot.getY()-this.y, 2)
                + Math.pow(dot.getZ()-this.z, 2));
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
