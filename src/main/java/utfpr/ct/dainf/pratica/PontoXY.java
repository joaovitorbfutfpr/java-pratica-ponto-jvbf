/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1949772
 */
public class PontoXY extends Ponto2D {

    public PontoXY() {
        super();
    }

    public PontoXY(double xi, double yi) {
        super(xi, yi, 0.0);
    }
    
    @Override
    public String toString(){
        return this.getNome()+String.format("(%f,%f)",this.getX(),this.getY());
    }
}
