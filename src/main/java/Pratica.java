
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ pxz = new PontoXZ(-3,2);
        PontoXY pxy = new PontoXY(0,2);
        System.out.print("Distancia = " + String.valueOf(pxz.dist(pxy)));
    }
    
}
